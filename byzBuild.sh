#!/bin/bash
rm -r temp
mkdir temp
cp BYZ-PRSD/* temp/
cd temp
sed -ri 's/<baqeov> 901/baqeov 901/g' JOH.UB5
sed -ri 's/M6: <monov 3441 \{A-NSM\}/M6:/g' JOH.UB5
sed -ri 's/o 3588 \{T-NSM\} ihsouv> 2424 \{N-NSM\} VAR:/ VAR:/g' JOH.UB5
sed -ri 's/\r//' *UB5
sed -ri 's/^     1:1( paulov 3972 \{N-NSM\} klhtov 2822 \{A-NSM\} apostolov 652)/\\id 1CO\n\\c 1\n\\p\n\\v 1\1/g' *UB5
sed -ri 's/^     1:1( o 3739 \{R-NSN\} hn 1510)/\\id 1JN\n\\c 1\n\\p\n\\v 1\1/g' *UB5
sed -ri 's/^     1:1( petrov 4074 \{N-NSM\} apostolov 652 \{N-NSM\} ihsou 2424)/\\id 1PE\n\\c 1\n\\p\n\\v 1\1/g' *UB5
sed -ri 's/^     1:1( paulov 3972 \{N-NSM\} kai 2532 \{CONJ\} silouanov 4610)/\\id 1TH\n\\c 1\n\\p\n\\v 1\1/g' *UB5
sed -ri 's/^     1:1( paulov 3972 \{N-NSM\} apostolov 652 \{N-NSM\} ihsou 2424)/\\id 1TI\n\\c 1\n\\p\n\\v 1\1/g' 1TI.UB5
sed -ri 's/^     1:1( paulov 3972 \{N-NSM\} apostolov 652 \{N-NSM\} ihsou 2424)/\\id  2CO\n\\c 1\n\\p\n\\v 1\1/g' 2CO.UB5
sed -ri 's/^     1:1( o 3588 \{T-NSM\} presbuterov 4245 \{A-NSM-C\} eklekth 1588)/\\id 2JN\n\\c 1\n\\p\n\\v 1\1/g' *UB5
sed -ri 's/^     1:1( sumewn 4826 \{N-PRI\} petrov 4074 \{N-NSM\} doulov 1401)/\\id 2PE\n\\c 1\n\\p\n\\v 1\1/g' *UB5
sed -ri 's/^     1:1( paulov 3972 \{N-NSM\} kai 2532 \{CONJ\} silouanov 4610)/\\id 2TH\n\\c 1\n\\p\n\\v 1\1/g' *UB5
sed -ri 's/^     1:1( paulov 3972 \{N-NSM\} apostolov 652 \{N-NSM\} ihsou 2424)/\\id 2TI\n\\c 1\n\\p\n\\v 1\1/g' 2TI.UB5
sed -ri 's/^     1:1( o 3588 \{T-NSM\} presbuterov 4245 \{A-NSM-C\} gaiw 1050)/\\id 3JN\n\\c 1\n\\p\n\\v 1\1/g' *UB5
sed -ri 's/^     1:1( ton 3588 \{T-ASM\} men 3303 \{PRT\} prwton 4413)/\\id ACT\n\\c 1\n\\p\n\\v 1\1/g' *UB5
sed -ri 's/^     1:1( paulov 3972 \{N-NSM\} apostolov 652 \{N-NSM\} ihsou 2424)/\\id COL\n\\c 1\n\\p\n\\v 1\1/g' COL.UB5
sed -ri 's/^     1:1( paulov 3972 \{N-NSM\} apostolov 652 \{N-NSM\} ihsou 2424)/\\id EPH\n\\c 1\n\\p\n\\v 1\1/g' EPH.UB5
sed -ri 's/^     1:1( paulov 3972 \{N-NSM\} apostolov 652 \{N-NSM\} ouk 3756)/\\id GAL\n\\c 1\n\\p\n\\v 1\1/g' *UB5
sed -ri 's/^     1:1( polumerwv 4181 \{ADV\} kai 2532 \{CONJ\} polutropwv 4187)/\\id HEB\n\\c 1\n\\p\n\\v 1\1/g' *UB5
sed -ri 's/^     1:1( iakwbov 2385 \{N-NSM\} qeou 2316 \{N-GSM\} kai 2532)/\\id JAS\n\\c 1\n\\p\n\\v 1\1/g' *UB5
sed -ri 's/^     1:1( en 1722 \{PREP\} arch 746 \{N-DSF\} hn 1510)/\\id JHN\n\\c 1\n\\p\n\\v 1\1/g' *UB5
sed -ri 's/^     1:1( ioudav 2455 \{N-NSM\} ihsou 2424 \{N-GSM\} cristou 5547)/\\id JUD\n\\c 1\n\\p\n\\v 1\1/g' *UB5
sed -ri 's/^     1:1( epeidhper 1895 \{CONJ\})/\\id LUK\n\\c 1\n\\p\n\\v 1\1/g' *UB5
sed -ri 's/^     1:1( arch 746 \{N-NSF\} tou 3588 \{T-GSN\} euaggeliou 2098)/\\id MRK\n\\c 1\n\\p\n\\v 1\1/g' *UB5
sed -ri 's/^     1:1( biblov 976 \{N-NSF\} genesewv 1078 \{N-GSF\} ihsou 2424)/\\id MAT\n\\c 1\n\\p\n\\v 1\1/g' *UB5
sed -ri 's/^     1:1( paulov 3972 \{N-NSM\} desmiov 1198 \{N-NSM\} cristou 5547)/\\id PHM\n\\c 1\n\\p\n\\v 1\1/g' *UB5
sed -ri 's/^     1:1( paulov 3972 \{N-NSM\} kai 2532 \{CONJ\} timoqeov 5095)/\\id PHP\n\\c 1\n\\p\n\\v 1\1/g' *UB5
sed -ri 's/^     1:1( apokaluyiv 602 \{N-NSF\} ihsou 2424 \{N-GSM\} cristou 5547)/\\id REV\n\\c 1\n\\p\n\\v 1\1/g' *UB5
sed -ri 's/^     1:1( paulov 3972 \{N-NSM\} doulov 1401 \{N-NSM\} ihsou 2424)/\\id ROM\n\\c 1\n\\p\n\\v 1\1/g' *UB5
sed -ri 's/^     1:1( paulov 3972 \{N-NSM\} doulov 1401 \{N-NSM\} qeou 2316)/\\id TIT\n\\c 1\n\\p\n\\v 1\1/g' *UB5

#sed -ri 's/^\{/@@/g' *UB5
sed -ri ':a;N;$!ba;s/\n([a-z0-9{|<:MVO])/ \1/g' *UB5

sed -ri 's/a/α/g' *UB5
sed -ri 's/b/β/g' *UB5
sed -ri 's/c/χ/g' *UB5
sed -ri 's/d/δ/g' *UB5
sed -ri 's/e/ε/g' *UB5
sed -ri 's/f/φ/g' *UB5
sed -ri 's/g/γ/g' *UB5
sed -ri 's/h/η/g' *UB5
sed -ri 's/i/ι/g' *UB5
sed -ri 's/k/κ/g' *UB5
sed -ri 's/l/λ/g' *UB5
sed -ri 's/m/μ/g' *UB5
sed -ri 's/n/ν/g' *UB5
sed -ri 's/o/ο/g' *UB5
sed -ri 's/p/π/g' *UB5
sed -ri 's/q/θ/g' *UB5
sed -ri 's/r/ρ/g' *UB5
sed -ri 's/s/σ/g' *UB5
sed -ri 's/t/τ/g' *UB5
sed -ri 's/u/υ/g' *UB5
sed -ri 's/v/ς/g' *UB5
sed -ri 's/w/ω/g' *UB5
sed -ri 's/x/ξ/g' *UB5
sed -ri 's/y/ψ/g' *UB5
sed -ri 's/z/ζ/g' *UB5
sed -ri 's/^\\χ 1/\\c 1/g' *UB5
sed -ri 's/^\\ιδ/\\id /g' *UB5
sed -ri 's/^\\ς 1 /\\v 1 /g' *UB5
sed -ri 's/^\\π/\\p/g' *UB5
sed -ri 's/^     ([0-9]*):1 /\\c \1\n\\p\n\\v 1 /g' *UB5
sed -ri 's/^     [0-9]*:([0-9]*)/\\v \1/g' *UB5
sed -ri 's/^     [0-9]*:([0-9]*)/\\v \1/g' *UB5

sed -ri 's/([α-ω]*) ([0-9]*) ([0-9]*) \{([A-Z0-9-]*)\}/\\w \1\|strong="G\2,G\3" x-morph="\4"\\w\*/g' *UB5
sed -ri 's/([α-ω]*) ([0-9]*) \{([A-Z0-9-]*)\}/ \\w \1\|strong="G\2" x-morph="\3"\\w\*/g' *UB5
sed -ri 's/([α-ω]*) ([0-9]*) ([0-9]*) \{([A-Z0-9-]*)\}/\\w \1\|strong="G\2,G\3" x-morph="\4"\\w\*/g' *UB5
sed -ri 's/([α-ω]*) ([0-9]*) \{([A-Z0-9-]*)\}/ \\w \1\|strong="G\2" x-morph="\3"\\w\*/g' *UB5
sed -ri 's/  / /g' *UB5
##Traiter les variants

##Todo reste deux entrée fautive jn 8,9.11 lié à include VAR et END mais aussi des erreurs dans variants, rechercher | dans le osis
#sed -ri 's/M6: ([^|].*) :M6 \|/\\var2 \1\\var2\*/g' *UB5
sed -ri 's/( x-morph="N-NSM"\\w\* | )< /\1/g' JOH.UB5
sed -ri 's/INCLUDE και υπο της συνειδησεως ελεγχομενοι :END//g' JOH.UB5
sed -ri 's/VAR: \\w.*[^END] :END :M6/:M6/g' JOH.UB5
sed -ri 's/M6:/\n\\var2/g' *UB5
sed -ri 's/M5:/\n&/g' *UB5
sed -ri 's/M5: [^M5].* :M5.*$//g' *UB5
sed -ri 's/VAR:/\n\\var2/g' *UB5
sed -ri 's/:END \||:M6 \|/\\var2\*/g' *UB5

sed -ri 's/\| (.*\\w\*) \| <.*>.*$/\\var1 \1\\var1\*/g' *UB5
sed -ri 's/\| ([^|].*\\w\*) \| *$/\\var1 \1\\var1\*/g' *UB5
sed -i '/^$/d' *.UB5
sed -i '382d' JOH.UB5
sed -i 's/M5: <αυτοφορω>\\w /\\var2 \\w αυτοφωρω/g' JOH.UB5
sed -i 's/M5: <μωσης> \\w /\\var2 \\w μωυσης/g' JOH.UB5

rename 's/UB5/usfm/g' *UB5
sed -ri 's/(\\w υμιν\|strong="G)(4771)/\15213/g' *usfm
sed -ri 's/(\\w υμεις\|strong="G)4771/\15210/g' *usfm
##Séparation des strongs et de la morph
sed -ri 's/,([A-Z1-3-]*)("\\)/" morph="robinson:\1\2/g' *usfm
u2o.py -e utf-8 -l grc -o byz.osis.xml -v -d BYZ -x *.usfm

mkdir ~/.sword/modules/texts/ztext/byznv/
#osis2mod ~/.sword/modules/texts/ztext/byznv/ byz.osis.xml -z
